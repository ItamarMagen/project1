#ifndef QUEUE_H
#define QUEUE_H
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

/* a queue contains positive integer values. */
typedef struct queue
{
	int * elements;
	int maxSize;
	int count;

} queue;

void initQueue(queue* q, unsigned int size);//create dynamic array according to maxsize
void cleanQueue(queue* q);//free the memory of the array

void enqueue(queue* q, unsigned int newValue);//insert element to the top of the stack
int dequeue(queue* q); //remove element from the top of the strack, return element in top of queue, or -1 if empty

bool isQueueFull(queue* q);
bool isQueueEmpty(queue* q);

#endif /* QUEUE_H */