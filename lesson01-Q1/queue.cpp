#include "queue.h"

bool isQueueFull(queue* q)
{
	return(q->maxSize == q->count);
}

bool isQueueEmpty(queue* q)
{
	return(q->count == 0);
}
void initQueue(queue* q, unsigned int size)
{
	q->elements = (int*)malloc(sizeof(int)*size);
	q->maxSize = size;
	q->count = 0;
}

void enqueue(queue* q, unsigned int newValue)
{
	if (!isQueueFull(q))
	{
		for (int i = q->count; i >= 0; i--)
		{
			if (i == 0)
			{
				q->elements[i] = newValue;
			}
			else
			{
				q->elements[i] = q->elements[i-1];
			}
		}
		q->count++;
	}
	else
	{
		std::cout << "The queue is full" << std::endl;
	}
}

int dequeue(queue* q)
{
	int topElement = 0;
	if (!isQueueEmpty)
	{
		topElement = q->elements[0];
		q->elements[0] = 0;
		for (int i = 1; i < q->count; i++)
		{
			q->elements[i - 1] = q->elements[i];
			q->elements[i] = 0;
		}
		q->count--;
	}
	else
		topElement = -1;
	return topElement;
}

void cleanQueue(queue* q)
{
	delete[] q->elements;
	delete(q);
}