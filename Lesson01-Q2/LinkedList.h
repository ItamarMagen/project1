#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct node
{
	int num;
	node* next;
}node;

typedef struct list
{
	node* head;
}list;

void addElement(list * lst, int num);//The function add the number to the of the list
void deleteElement(list* lst);//The function delete the first element of the list
void printList(list* lst);//The function print the list

#endif /* LINKEDLIST_H */