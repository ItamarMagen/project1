#include "stack.h"
#include <iostream>

void push(stack* s, unsigned int element)
{
	stack *temp = new stack;
	temp->num = element;
	temp->next = s;
	s = temp;
}

int pop(stack* s)
{
	int ans = -1;
	stack *temp = new stack;
	if (s)
	{
		temp = s;
		s = s->next;
		ans = temp->num;
		delete temp;
	}
	return ans;
}
void initStack(stack* s)
{
	s = new stack;
}

void cleanStack(stack* s)
{	
	stack* temp = new stack;
	while (s)
	{
		temp = s;
		delete(temp);
		s = s->next;
	}
	delete(s);
}

void printStack(stack*s)
{
	while (s)
	{
		std::cout << s->num << std::endl;
		s = s->next;
	}
}