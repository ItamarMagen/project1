#include "LinkedList.h"
#include <iostream>


void addElement(list * lst, int num)
{
	node *temp = new node;
	temp->num = num;
	temp->next = lst->head;
	lst->head = temp;
}

void deleteElement(list* lst)
{
	node *temp = new node;
	temp = lst->head;
	lst->head = lst->head->next;
	delete temp;
}

void printList(list* lst)
{
	node * curr = lst->head;
	while (curr!=NULL)
	{
		std::cout << curr->num << std::endl;
		curr = curr->next;
	}
}