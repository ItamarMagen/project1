#include "utils.h"
#include <iostream>

void reverse(int* nums, unsigned int size)
{
	int temp, i;
	for (i = 0; i < size / 2; ++i)
	{
		temp = nums[i];
		nums[i] = nums[size - i - 1];
		nums[size - i - 1] = temp;
	}
}

int* reverse10()
{
	int* arr = new int[10];
	int i = 0;
	std::cout << "Enter 10 NUmbers:" << std::endl;
	for (i = 9; i >= 0; i--)
	{
		std::cin >> arr[i];
	}
	return arr;
}